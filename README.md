# LoRa Geolocation

## Overview

### Owners & Administrators

| Title       | Details                                                                      |
|-------------|------------------------------------------------------------------------------|
| **E-group** | [lpwan-admins](https://groups-portal.web.cern.ch/group/lpwan-admins/details) |
| **People**  | [Alessandro Zimmaro](https://phonebook.cern.ch/search?q=Alessandro+Zimmaro)  |

### Kafka Topics

| Environment    | Topic Name                                                                                                        |
|----------------|-------------------------------------------------------------------------------------------------------------------|
| **Production** | [lora-geolocation](https://nile-kafka-ui.app.cern.ch/ui/clusters/gp3/all-topics/lora-geolocation)                 |
| **Production** | [lora-geolocation-decoded](https://nile-kafka-ui.app.cern.ch/ui/clusters/gp3/all-topics/lora-geolocation-decoded) |
| **QA**         | [lora-geolocation](https://nile-kafka-ui.app.cern.ch/ui/clusters/qa3/all-topics/lora-geolocation)                 |
| **QA**         | [lora-geolocation-decoded](https://nile-kafka-ui.app.cern.ch/ui/clusters/qa3/all-topics/lora-geolocation-decoded) |

### Configuration

| Title                        | Details                                                                                          |
|------------------------------|--------------------------------------------------------------------------------------------------|
| **Application Name**         | cern-rme-geoloc                                                                                  |
| **Configuration Repository** | [app-configs/lora-geolocation](https://gitlab.cern.ch/nile/streams/app-configs/lora-geolocation) |
