package ch.cern.nile.app;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.time.DateTimeException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.DatatypeConverter;

import com.google.gson.JsonObject;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;

import ch.cern.nile.common.json.JsonSerde;
import ch.cern.nile.common.streams.AbstractStream;
import ch.cern.nile.common.streams.StreamUtils;
import ch.cern.nile.common.streams.offsets.InjectOffsetProcessorSupplier;

public final class GeolocationStream extends AbstractStream {

    @Override
    public void createTopology(final StreamsBuilder builder) {
        builder
                .stream(getSourceTopic(), Consumed.with(Serdes.String(), new JsonSerde()))
                .filter(this::filterRecord)
                .processValues(new InjectOffsetProcessorSupplier(), InjectOffsetProcessorSupplier.getSTORE_NAME())
                .mapValues(this::mapValues)
                .filter(StreamUtils::filterNull)
                .to(getSinkTopic());
    }

    private boolean filterRecord(final String ignored, final JsonObject value) {
        return value != null && value.get("data") != null && value.get("fPort") != null
                && value.get("deviceName") != null && value.get("rxInfo") != null;
    }

    private Map<String, Object> mapValues(final JsonObject value) {
        final Map<String, Object> map = new HashMap<>();

        decodePayload(value.get("data").getAsString(), map);
        decorateId(value.get("deviceName").getAsString(), map);

        try {
            StreamUtils.addTimestamp(value.get("rxInfo").getAsJsonArray(), map);
            setLastReadOffset(value.get("offset").getAsLong());
        } catch (DateTimeException e) {
            logStreamsException(e);
            map.clear();
        }
        return map;
    }

    private void decorateId(final String devId, final Map<String, Object> dict) {
        dict.put("devId", devId);
    }

    private void decodePayload(final String payload, final Map<String, Object> dict) {
        try {
            final byte[] decode = DatatypeConverter.parseBase64Binary(payload);
            final ByteBuffer buffer = ByteBuffer.wrap(decode).order(ByteOrder.LITTLE_ENDIAN);
            while (buffer.hasRemaining()) {
                final int latitudeRaw = buffer.getInt();
                final int longitudeRaw = buffer.getInt();
                final int headingRaw = buffer.get() & 0xFF;
                final int speed = buffer.get() & 0xFF;
                final int voltageRaw = buffer.get() & 0xFF;
                dict.put("type", "position");
                dict.put("latitudeDeg", latitudeRaw * 1e-7);
                dict.put("longitudeDeg", longitudeRaw * 1e-7);
                dict.put("inTrip", (headingRaw & 1) != 0);
                dict.put("fixFailed", (headingRaw & 2) != 0);
                dict.put("speedKmph", speed);
                dict.put("headingDeg", (headingRaw >> 2) * 5.625);
                dict.put("batV", voltageRaw * 0.025);
                dict.put("manDown", false);
            }
        } catch (IllegalArgumentException e) {
            logStreamsException(e);
        }
    }

}
